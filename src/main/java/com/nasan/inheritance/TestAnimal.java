/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.inheritance;

/**
 *
 * @author nasan
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Animal01", "black", 4);
        animal.walk();
        animal.speak();

        Dog to = new Dog("To", "orange");
        to.walk();
        to.speak();
        Dog dang = new Dog("Dang", "black&white");
        dang.walk();
        dang.speak();
        Dog mome = new Dog("Mome", "black&white");
        mome.walk();
        mome.speak();
        Dog bat = new Dog("Bat", "black&white");
        bat.walk();
        bat.speak();

        Cat zero = new Cat("Zero", "orange");
        zero.walk();
        zero.speak();

        Duck som = new Duck("Som", "orange");
        som.walk();
        som.fly();
        som.speak();
        Duck gabgab = new Duck("GabGab", "orange");
        gabgab.walk();
        gabgab.fly();
        gabgab.speak();

        System.out.println("_____Check_____");

        checkOfAnimal01(animal);
        checkOfTo(to);
        checkOfDang(dang);
        checkOfMome(mome);
        checkOfBat(bat);
        checkOfZero(zero);
        checkOfSom(som);
        checkOfGabgab(gabgab);
        
        System.out.println("_____Can fly ?_____");
        
        canFly(animal, to, dang, mome, bat, zero, som, gabgab);

    }

    public static void canFly(Animal animal, Dog to, Dog dang, Dog mome, Dog bat, Cat zero, Duck som, Duck gabgab) {
        Animal[] animals = {animal,to,dang,mome,bat,zero,som,gabgab};
        for(int i=0;i<animals.length;i++){
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }else
                System.out.println(animals[i].name+" Can't fly ;-;");
        }
    }

    public static void checkOfGabgab(Duck gabgab) {
        System.out.println("GabGab is Animal: " + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck));
        System.out.println("GabGab is Object: " + (gabgab instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfSom(Duck som) {
        System.out.println("Som is Animal: " + (som instanceof Animal));
        System.out.println("Som is Duck: " + (som instanceof Duck));
        System.out.println("Som is Object: " + (som instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfZero(Cat zero) {
        System.out.println("Zero is Animal: " + (zero instanceof Animal));
        System.out.println("Zero is Cat: " + (zero instanceof Cat));
        System.out.println("Zero is Object: " + (zero instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfBat(Dog bat) {
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is Dog: " + (bat instanceof Dog));
        System.out.println("Bat is Object: " + (bat instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfMome(Dog mome) {
        System.out.println("Mome is Animal: " + (mome instanceof Animal));
        System.out.println("Mome is Dog: " + (mome instanceof Dog));
        System.out.println("Mome is Object: " + (mome instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfDang(Dog dang) {
        System.out.println("Dang is Animal: " + (dang instanceof Animal));
        System.out.println("Dang is Dog: " + (dang instanceof Dog));
        System.out.println("Dang is Object: " + (dang instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfTo(Dog to) {
        System.out.println("To is Animal: " + (to instanceof Animal));
        System.out.println("To is Dog: " + (to instanceof Dog));
        System.out.println("To is Object: " + (to instanceof Object));
        System.out.println("__________");
    }

    public static void checkOfAnimal01(Animal animal) {
        System.out.println("Animal01 is Animal: " + (animal instanceof Animal));
        System.out.println("Animal01 is Dog: " + (animal instanceof Dog));
        System.out.println("Animal01 is Cat: " + (animal instanceof Cat));
        System.out.println("Animal01 is Duck: " + (animal instanceof Duck));
        System.out.println("Animal01 is Object: " + (animal instanceof Object));
        System.out.println("__________");
    }
}
