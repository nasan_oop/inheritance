/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.inheritance;

/**
 *
 * @author nasan
 */
public class Dog extends Animal{
    public Dog(String name,String color){
        super(name,color,4);
        System.out.println("Dog created");
    }
    
    @Override
    public void walk (){
        System.out.println("Dog name: "+this.name+" walk with "+this.NumberOfLegs+" legs");
    }
    
    @Override
    public void speak(){
        System.out.println("Dog name: "+this.name +" it has "+this.color+" color" +" speak box box!!");
        System.out.println("_________________________________________");
    }
}
