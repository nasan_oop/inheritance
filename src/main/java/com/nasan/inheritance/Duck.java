/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.inheritance;

/**
 *
 * @author nasan
 */
public class Duck extends Animal {
    private int NumberOfWings = 2;
    public Duck (String name,String color){
        super(name,color,2);
        System.out.println("Duck Created");
    }
    
    @Override
    public void walk (){
        System.out.println("Duck name: "+this.name+" walk with "+this.NumberOfLegs+" legs");
    }
    
    @Override
    public void speak(){
        System.out.println("Duck name: "+this.name +" it has "+this.color+" color" +" speak gap gap!!");
        System.out.println("_________________________________________");
    }
    
    public void fly(){
        System.out.println("Duck name: "+this.name+" Fly with "+this.NumberOfWings+" wings");

    }
}
