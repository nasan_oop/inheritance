/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.inheritance;

/**
 *
 * @author nasan
 */
public class Animal {
    protected String name;
    protected int NumberOfLegs;
    protected String color;
    
    public Animal (String name,String color,int NumberOfLegs){
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.NumberOfLegs = NumberOfLegs;
    }
    
    public void walk (){
        System.out.println("Animal walk");
    }
    
    public void speak(){
        System.out.println("Animal speak");
        System.out.println("name: "+this.name + " color: "+this.color+" number of legs: "+this.NumberOfLegs);
        System.out.println("_________________________________________");
    }

    
    
}
